﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    [SerializeField]
    private GameObject _tower;


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "plane")
        {
            if(GameMenadzerScript.Instance.GetCurrentCounterAmount() <= 100 && !GameMenadzerScript.Instance._IsAmountOfTowersReachLimit)
            {
                if (GameMenadzerScript.Instance.GetCurrentCounterAmount() == 100)
                {               
                    GameMenadzerScript.Instance._IsAmountOfTowersReachLimit = true;
                }
                GameObject newTower = Instantiate(_tower, transform.position + new Vector3(0, 1.5f, 0), new Quaternion(0, 0, 0, 1));
                GameMenadzerScript.Instance._listOfTowers.Add(newTower);
                GameMenadzerScript.Instance.AddOneToTowerCounter();
            }        
            this.gameObject.SetActive(false);
        }

        if (other.tag == "tower")
        {      
            Destroy(other.gameObject.transform.parent.gameObject);
            GameMenadzerScript.Instance.SubstractOneToTowerCounter();
            this.gameObject.SetActive(false);
        }
    }
}
