﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingletonFile;

public class TowerScript : Singleton<TowerScript> {

    //Places for bullets
    [SerializeField]
    private Transform _pointToInstantiateBulletTransform;
    [SerializeField]
    private Transform _arsenalTransform;
    [SerializeField]
    private MeshRenderer _bodyOfTower;

    [SerializeField]
    private Material _activeTowerMaterial;
    [SerializeField]
    private Material _unActiveTowerMaterial;

    public bool _amIFirstTowerInScene = false;

    private bool _firstIteration = true;
    public int _bulletInstantietedCounter = 0;

    void Start ()
    {
        StartCoroutine("RotateAndShoot");
    }

    public void TurnOnProcess()
    {
        StartCoroutine("RotateAndShoot");
        _bulletInstantietedCounter = 0;
    }

	IEnumerator RotateAndShoot()
    {
       

        if (_firstIteration && !_amIFirstTowerInScene)
        {
            yield return new WaitForSeconds(6);
            _firstIteration = false;

            _bodyOfTower.material = _activeTowerMaterial;
            _arsenalTransform.GetComponent<MeshRenderer>().material = _activeTowerMaterial;
        }

        yield return new WaitForSeconds(0.5f);

        float currentY = transform.rotation.y;
        transform.Rotate(0, currentY + Random.Range(15,45), 0);

        GameObject bullet = ObjectPooling.Instance.SpawnFromPool("Bullet", _pointToInstantiateBulletTransform.position, new Quaternion(0, 0, 0, 1));
        bullet.GetComponent<Rigidbody>().AddForce(600 * Random.Range(1, 4) * ReturnUnitVector(_arsenalTransform.position,_pointToInstantiateBulletTransform.position));
        _bulletInstantietedCounter++;

        if(_bulletInstantietedCounter < 12)
        {
            StartCoroutine("RotateAndShoot");
        }
        else
        {
            _bodyOfTower.material = _unActiveTowerMaterial;
            _arsenalTransform.GetComponent<MeshRenderer>().material = _unActiveTowerMaterial;
        }
       
    }

    private Vector3 ReturnUnitVector(Vector3 A, Vector3 B)
    {
        return (B - A) / Vector3.Distance(A, B);
    }
}
