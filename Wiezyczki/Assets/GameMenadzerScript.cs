﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SingletonFile;
using UnityEngine.UI;

public class GameMenadzerScript : Singleton<GameMenadzerScript> {

    [SerializeField]
    private Text _counterOfInstantiatedTowers;

    public List<GameObject> _listOfTowers;
    public bool _IsAmountOfTowersReachLimit = false;

    public void ActiveProcess()
    {
        foreach (var item in _listOfTowers)
        {
            item.GetComponent<TowerScript>().TurnOnProcess();
        }
    }

    public void AddOneToTowerCounter()
    {
        int counter = int.Parse(_counterOfInstantiatedTowers.text);
        counter++;
        _counterOfInstantiatedTowers.text = counter.ToString();
    }

    public void SubstractOneToTowerCounter()
    {
        int counter = int.Parse(_counterOfInstantiatedTowers.text);
        counter--;
        _counterOfInstantiatedTowers.text = counter.ToString();
    }

    public int GetCurrentCounterAmount()
    {
        return int.Parse(_counterOfInstantiatedTowers.text);
    }
}
